import {Injectable} from "@angular/core";
import {Observable} from "rxjs/Observable";
import {ICart} from "../models/ICart";
import {Http} from "@angular/http";
import {config} from "../../config/config.dev";
import "rxjs/add/operator/map";
@Injectable()
export class CartService {
  constructor(private http: Http) {
  }

  getCart(): Observable<ICart> {
    return this.http
      .get(`${config.api}cart`)
      .map(res => {
        const data = res.json();
        return <ICart>data;
      });
  }
}
