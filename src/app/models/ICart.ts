import {IItem} from "./IItem";
import {IPrice} from "./IPrice";
export interface ICart {
  items: IItem[];
  summary: IPrice[];
}
