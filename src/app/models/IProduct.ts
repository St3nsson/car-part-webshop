import {IPrice} from "./IPrice";
export interface IProduct {
  id: number;
  title: string;
  imageUrl: string;
  url: string;
  prices: IPrice[];
}
