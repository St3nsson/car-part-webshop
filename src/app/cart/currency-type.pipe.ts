import {Pipe, PipeTransform} from '@angular/core';

@Pipe({
  name: 'currencyType'
})
export class CurrencyTypePipe implements PipeTransform {

  transform(value: string): string {
    switch (value) {
      case "SEK":
        return "kr";
      case "EUR":
        return "€";
      default:
        return ""
    }
  }

}
