import {Component, OnInit} from '@angular/core';
import {ICart} from "../models/ICart";
import {CartService} from "../services/Cart.service";
import {IPrice} from "../models/IPrice";

@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.css']
})
export class CartComponent implements OnInit {

  cart: ICart;
  invoiceCost: IPrice = {amount: 29, currency: "SEK"};

  constructor(private cartService: CartService) {
  }

  ngOnInit() {
    this.cartService
      .getCart()
      .subscribe(res => {
        console.log(res);
        this.cart = res;
      });
  }
}
