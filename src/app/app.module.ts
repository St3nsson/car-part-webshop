import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { AddressComponent } from './address/address.component';
import { CartComponent } from './cart/cart.component';
import { DeliveryComponent } from './delivery/delivery.component';
import { PaymentComponent } from './payment/payment.component';
import {HttpModule} from "@angular/http";
import {CartService} from "./services/Cart.service";
import { ItemComponent } from './item/item.component';
import { CurrencyTypePipe } from './cart/currency-type.pipe';

@NgModule({
  declarations: [
    AppComponent,
    AddressComponent,
    CartComponent,
    DeliveryComponent,
    PaymentComponent,
    ItemComponent,
    CurrencyTypePipe,
  ],
  imports: [
    BrowserModule,
    HttpModule,
  ],
  providers: [CartService],
  bootstrap: [AppComponent]
})
export class AppModule { }
