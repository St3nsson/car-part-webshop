import { CarPartWebshopPage } from './app.po';

describe('car-part-webshop App', () => {
  let page: CarPartWebshopPage;

  beforeEach(() => {
    page = new CarPartWebshopPage();
  });

  it('should display welcome message', done => {
    page.navigateTo();
    page.getParagraphText()
      .then(msg => expect(msg).toEqual('Welcome to app!!'))
      .then(done, done.fail);
  });
});
